package usecases_test

import (
	"fmt"
	"testing"

	"github.com/abdullahkaraman/task-user-management/backend/entities"
	"github.com/abdullahkaraman/task-user-management/backend/usecases"
	"github.com/gomagedon/expectate"
)

var dummyUsers = []entities.User{
	{
		Id:   12,
		Name: "Ahmet",
		Mail: "deneme@mail.com",
		Age:  "19",
	},
	{
		Id:   22,
		Name: "Mehmet",
		Mail: "deneme2@mail.com",
		Age:  "32",
	},
	{
		Id:   12,
		Name: "Ali",
		Mail: "deneme3@mail.com",
		Age:  "44",
	},
}

type BadUsersRepo struct{}

func (BadUsersRepo) GetAllUsers() ([]entities.User, error) {
	return nil, fmt.Errorf("smth went wrong")
}

type MockUsersRepo struct{}

func (MockUsersRepo) GetAllUsers() ([]entities.User, error) {
	return dummyUsers, nil
}
func TestGetUsers(t *testing.T) {
	t.Run("Returns ErrInternal when UsersRepository returns error", func(t *testing.T) {
		expect := expectate.Expect(t)

		repo := new(BadUsersRepo)

		users, err := usecases.GetUsers(repo)

		expect(err).ToBe(usecases.ErrInternal)
		// if err != usecases.ErrInternal {
		// 	t.Fatalf("Expected ErrInt; Got: %v", err)
		// }

		if users != nil {
			t.Fatalf("Expected users to be nil; Got: %v", users)
		}
	})
	t.Run("Returns users from UsersRepository", func(t *testing.T) {
		expect := expectate.Expect(t)

		repo := new(MockUsersRepo)

		users, err := usecases.GetUsers(repo)

		expect(err).ToBe(nil)
		expect(users).ToEqual(dummyUsers)
	})

}
