package usecases

import "github.com/abdullahkaraman/task-user-management/backend/entities"

func GetUsers(repo UsersRepository) ([]entities.User, error) {
	users, err := repo.GetAllUsers()
	if err != nil {
		return nil, ErrInternal
	}
	return users, nil
}
