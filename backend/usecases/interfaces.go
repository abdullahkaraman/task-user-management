package usecases

import "github.com/abdullahkaraman/task-user-management/backend/entities"

type UsersRepository interface {
	GetAllUsers() ([]entities.User, error)
}
