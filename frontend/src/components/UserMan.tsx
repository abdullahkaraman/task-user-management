import React from "react";
import { User } from "../entities/User";

type Props = {
    users: User[]
}

export const UserMan: React.FC<Props> = ({ users }) => {
    return (
        <ul className="user-man">
            {
                users.map((user, i) => (
                    <li key={i}>{user.name}</li>
                ))
            }
        </ul>
    )
}