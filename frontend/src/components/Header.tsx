import './Header.scss';

export const Header = () => {
    return (
        <header className="page-header">
            <h1>User Management App</h1>
        </header>
    )
}