import "./Form.scss";

export default function Form() {
  return (
    <div className="form-container">
      <form className="register-form">
        <input
          id="full-name"
          className="form-field"
          type="text"
          placeholder="Full Name"
          name="fullName"
        />
        <input
          id="mail"
          className="form-field"
          type="text"
          placeholder="Email"
          name="mail"
        />
        <input
          id="age"
          className="form-field"
          type="text"
          placeholder="Age"
          name="age"
        />
        <button className="form-field" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}