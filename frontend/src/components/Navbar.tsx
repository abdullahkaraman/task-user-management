import  Navbar from 'react-bootstrap/Navbar';
import { Container, Nav } from 'react-bootstrap';

export default function Navi() {
    return (
<Navbar bg="primary" variant="dark">
<Container>
<Nav className="me-auto">
  <Nav.Link href="#home">Home</Nav.Link>
  <Nav.Link href="#features">Features</Nav.Link>
  <Nav.Link href="#pricing">Pricing</Nav.Link>
</Nav>
</Container>
</Navbar>
    )
}