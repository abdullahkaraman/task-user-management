import React from 'react';
import { Header } from './components/Header';
import { UserMan } from './components/UserMan';
import './App.css';
import Form from './components/Form';
import Navi from './components/Navbar';


function App() {
  return (
    <div className="App">
      <Header />
      <Navi />
      {/* <UserMan users={[
      {name:"Deneme"},
      {name:"Deneme2"},
      ]} /> */}
      <Form />
    </div>
  );
}

export default App;
